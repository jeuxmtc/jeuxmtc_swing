package metier;

public class Pair {
	public Pair(int key, Element value) {
		this.key = key;
		this.value = value;
	}
	
	public int key;
	public Element value;
}
