package IHM;

import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JFrame;
import javax.swing.JPanel;

import metier.Element;
import metier.Pair;

/**
 *
 * @author Alexandre
 */
public class Scores extends javax.swing.JPanel {
    private Frame frame;
    private ArrayList<Float> listeS;
    
    public Scores(Frame f) {
        initComponents();
        frame = f;
        listeS = new ArrayList<Float>();
        listeS = frame.getScores();
        Collections.sort(listeS, Collections.reverseOrder()); 
		for (int i = 0; i < frame.getScores().size(); i++) {
			setLabelScores(i+1,listeS.get(i));
		}
    }
    
    
    public void setLabelScores (int i, float s)
    {
    	if (i==1)
    		labelScore1.setText("" + s);
    	if (i==2)
    		labelScore2.setText("" + s);
    	if (i==3)
    		labelScore3.setText("" + s);
    	if (i==4)
    		labelScore4.setText("" + s);
    	if (i==5)
    		labelScore5.setText("" + s);
    }



    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        labelScore5 = new javax.swing.JLabel();
        labelScore3 = new javax.swing.JLabel();
        labelScore4 = new javax.swing.JLabel();
        labelScore2 = new javax.swing.JLabel();
        labelScore1 = new javax.swing.JLabel();
        labelScore6 = new javax.swing.JLabel();
        labelScore7 = new javax.swing.JLabel();
        labelScore8 = new javax.swing.JLabel();
        labelScore9 = new javax.swing.JLabel();
        labelScore10 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(222, 41, 16));
        setPreferredSize(new java.awt.Dimension(1280, 720));
        setLayout(null);

        labelScore5.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        labelScore5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        add(labelScore5);
        labelScore5.setBounds(950, 540, 240, 70);

        labelScore3.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        labelScore3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        add(labelScore3);
        labelScore3.setBounds(950, 360, 240, 70);

        labelScore4.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        labelScore4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        add(labelScore4);
        labelScore4.setBounds(950, 450, 240, 70);

        labelScore2.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        labelScore2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        add(labelScore2);
        labelScore2.setBounds(950, 270, 240, 70);

        labelScore1.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        labelScore1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        add(labelScore1);
        labelScore1.setBounds(950, 180, 240, 70);

        labelScore6.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        labelScore6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelScore6.setText("5 -");
        add(labelScore6);
        labelScore6.setBounds(860, 540, 80, 70);

        labelScore7.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        labelScore7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelScore7.setText("4 -");
        add(labelScore7);
        labelScore7.setBounds(860, 450, 80, 70);

        labelScore8.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        labelScore8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelScore8.setText("3 -");
        add(labelScore8);
        labelScore8.setBounds(860, 360, 80, 70);

        labelScore9.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        labelScore9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelScore9.setText("2 -");
        add(labelScore9);
        labelScore9.setBounds(860, 270, 80, 70);

        labelScore10.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        labelScore10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelScore10.setText("1 -");
        add(labelScore10);
        labelScore10.setBounds(860, 180, 80, 70);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/retourMenu.png"))); // NOI18N
        jButton1.setBorder(null);
        jButton1.setContentAreaFilled(false);
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1);
        jButton1.setBounds(0, 0, 161, 60);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/EcranScore.png"))); // NOI18N
        add(jLabel1);
        jLabel1.setBounds(10, 12, 1260, 680);
    }// </editor-fold>                        

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        JPanel p = new Menu(frame);
        frame.setPanel(p);
    }                                        


    // Variables declaration - do not modify                     
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel labelScore1;
    private javax.swing.JLabel labelScore10;
    private javax.swing.JLabel labelScore2;
    private javax.swing.JLabel labelScore3;
    private javax.swing.JLabel labelScore4;
    private javax.swing.JLabel labelScore5;
    private javax.swing.JLabel labelScore6;
    private javax.swing.JLabel labelScore7;
    private javax.swing.JLabel labelScore8;
    private javax.swing.JLabel labelScore9;
    // End of variables declaration                   
}

