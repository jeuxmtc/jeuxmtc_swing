package IHM;


import java.awt.Panel;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import WebService.BadCredentialsException;

/**
 *
 * @author Alexandre
 */
public class Connexion extends javax.swing.JPanel {
    Frame frame;

    public Connexion(Frame f) {
        initComponents();
        frame = f;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        ButtonConnexion = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldLogin = new javax.swing.JTextField();
        PasswordField = new javax.swing.JPasswordField();

        setBackground(new java.awt.Color(222, 41, 16));
        setLayout(null);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/Roue_elements.png"))); // NOI18N
        add(jLabel1);
        jLabel1.setBounds(54, 12, 672, 667);

        ButtonConnexion.setBackground(new java.awt.Color(222, 41, 16));
        ButtonConnexion.setForeground(new java.awt.Color(222, 41, 16));
        ButtonConnexion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/connexion_FR.png"))); // NOI18N
        ButtonConnexion.setBorder(null);
        ButtonConnexion.setBorderPainted(false);
        ButtonConnexion.setContentAreaFilled(false);
        ButtonConnexion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ButtonConnexion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try {
					ButtonConnexionActionPerformed(evt);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });
        add(ButtonConnexion);
        ButtonConnexion.setBounds(820, 460, 413, 130);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/Logo_Jeu.png"))); // NOI18N
        add(jLabel2);
        jLabel2.setBounds(800, 20, 432, 199);

        jTextFieldLogin.setText("toto");
        jTextFieldLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldLoginActionPerformed(evt);
            }
        });
        add(jTextFieldLogin);
        jTextFieldLogin.setBounds(890, 290, 279, 36);
        jTextFieldLogin.getAccessibleContext().setAccessibleName("name");
        jTextFieldLogin.getAccessibleContext().setAccessibleDescription("descroption");

        PasswordField.setText("DeOOOOufeeeazeaze");
        PasswordField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PasswordFieldActionPerformed(evt);
            }
        });
        add(PasswordField);
        PasswordField.setBounds(890, 360, 279, 36);
    }// </editor-fold>                        

    private void ButtonConnexionActionPerformed(java.awt.event.ActionEvent evt) throws IOException {
    	
		try{
			this.frame.setCurrentUser(
					this.frame.getWebApi().Login(this.jTextFieldLogin.getText(), this.PasswordField.getPassword())
			);
			
		    JPanel p = new Menu(frame);
		    frame.setPanel(p);
		}catch(BadCredentialsException e){
			JOptionPane.showMessageDialog(this.frame, e.getMessage(), "Identifiants incorrects",JOptionPane.ERROR_MESSAGE);
		}

    }                                               

    private void PasswordFieldActionPerformed(java.awt.event.ActionEvent evt) {                                              
        // TODO add your handling code here:
    }                                             

    private void jTextFieldLoginActionPerformed(java.awt.event.ActionEvent evt) {                                                
        // TODO add your handling code here:
    }                                               


    // Variables declaration - do not modify                     
    private javax.swing.JButton ButtonConnexion;
    private javax.swing.JPasswordField PasswordField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField jTextFieldLogin;
    // End of variables declaration                   
}
