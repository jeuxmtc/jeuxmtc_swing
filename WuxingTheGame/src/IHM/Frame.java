package IHM;


import java.awt.Color;
import java.net.MalformedURLException;
import java.util.ArrayList;

import javax.swing.JPanel;

import WebService.WebApi;
import metier.Element;
import metier.Pair;
import metier.Question;
import metier.User;

/**
 *
 * @author Alexandre
 */
public class Frame extends javax.swing.JFrame {
	private ArrayList<Float> scores;
	private WebApi webApi;
	private User currentUser = null;

    public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public WebApi getWebApi() {
		return webApi;
	}

	public Frame() throws MalformedURLException {
        initComponents();
        this.setResizable(false);
        setContentPane(new Connexion(this));     
        this.setVisible(true);
        scores = new ArrayList<Float>();
        this.webApi = new WebApi();
    }
    
    public Question getQuestion1() {
        Question question = new Question(1);
        question.setIntitule("Placer les saisons");
        question.setExplaination("Explanations?");

        //Cases
        Element e1 = new Element();
        e1.setLibelle("Feu");
        e1.setUrl_image("/ressources/Feu");
        e1.setID(1);
        question.addFixedElement(e1);
		
        Element e2 = new Element();
        e2.setLibelle("Eau");
        e2.setUrl_image("/ressources/Eau");
        e2.setID(2);
        question.addFixedElement(e2);
		
        Element e3 = new Element();
        e3.setLibelle("Terre");
        e3.setUrl_image("/ressources/Terre");
        e3.setID(3);
        question.addFixedElement(e3);
		
        Element e4 = new Element();
        e4.setLibelle("Eau");
        e4.setUrl_image("/ressources/M�tal");
        e4.setID(4);
        question.addFixedElement(e4);
		
        Element e5 = new Element();
        e5.setLibelle("Bois");
        e5.setUrl_image("/ressources/Bois");
        e5.setID(5);
        question.addFixedElement(e5);
		
	//Mots � placer
	Element e6 = new Element();
        e6.setLibelle("Rate");
        e6.setUrl_image("/ressources/Rate");
	question.addMovableElement(1, e6);
		
	Element e7 = new Element();
        e7.setLibelle("Poumon");
        e7.setUrl_image("/ressources/Poumon");
	question.addMovableElement(2, e7);
		
	Element e8 = new Element();
        e8.setLibelle("Reins");
        e8.setUrl_image("/ressources/Rein");
	question.addMovableElement(3, e8);
		
	Element e9 = new Element();
        e9.setLibelle("Coeur");
        e9.setUrl_image("/ressources/Coeur");
	question.addMovableElement(4, e9);
		
        return question;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1280, 720));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 714, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 398, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>                        

    public void setPanel (JPanel p)
    {
        this.setContentPane(p);
        this.setVisible(true);
    }
    
	public void addscore(float s) {
		scores.add(s);
	}
	
	public ArrayList<Float> getScores() {
		return scores;
	}

    // Variables declaration - do not modify                     
    // End of variables declaration                   
}
