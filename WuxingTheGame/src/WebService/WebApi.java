package WebService;

import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import metier.Question;
import metier.User;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.stream.JsonReader;

public class WebApi {

	private Gson gson = new Gson();
	private URL baseUri ;
	
	public WebApi() throws MalformedURLException {
			baseUri = new URL("http://kai23.fr:9000");		
			
			//Définition d'un adapter pour les réponses du web service dont la forme est variable
			IServeurResponseAdapter<User> adapter =  new IServeurResponseAdapter<User>(User.class);

		    {
		        GsonBuilder builder = new GsonBuilder();
		        builder.registerTypeAdapter(IApiResponse.class   ,adapter);
		        gson = builder.create();
		    }
		
	}
	
	@SuppressWarnings("resource")
	public ArrayList<Question> getQuestion(int nb) throws IOException {
		ArrayList<Question> questionsResult = new ArrayList<Question>();
		
		if(nb < 1 || nb > 40) 
			throw new NumberFormatException("The number of question supplied must be between 1 and 40");
		
		URL questionUrl = new URL(baseUri, "/questions?nb=" + nb);
		JsonReader reader = new JsonReader(new InputStreamReader(questionUrl.openStream()));
		
		try{
			reader.beginArray();
			
			  while(reader.hasNext())
			   {
			      Question q = gson.fromJson(reader, Question.class);
			      questionsResult.add(q);
			   }
		}
		finally{
			   reader.endArray();
			   reader.close();
		}
		
		return questionsResult;
	}
	
	@SuppressWarnings("resource")
	public ArrayList<Question> getQuestion() throws IOException {

		return this.getQuestion(15);
	}
	
	public User Login(String usrName, String usrPwd) throws IOException{
		ApiResponse<User> resp;
		
		if(usrName == null || usrName.isEmpty())
			throw new BadCredentialsException("An email has to be provided");
		
		if(usrPwd == null || usrPwd.isEmpty())
			throw new BadCredentialsException("A password has to be provided");
		
		URL loginUrl = new URL(baseUri, "/login_user?username=" + usrName + "&password=" + usrPwd );
		JsonReader reader = new JsonReader(new InputStreamReader(loginUrl.openStream()));
		
		try{
		  resp = gson.fromJson(reader, IApiResponse.class);
		}
		finally{
			reader.close();
		}
		

		if(!resp.isSuccess())
			throw new BadCredentialsException(resp.getMessage());
		
		return resp.getObject();
	}
	
	public User Login(String usrName, char[] usrPwd) throws IOException{
		return this.Login(usrName, new String(usrPwd));
	}

	public void createUser(String usrEmail, String usrPwd, String mail) throws IOException{
		ApiResponse resp;
		
		if(usrEmail == null || usrEmail.isEmpty())
			throw new InvalidRegistrationException("An username has to be provided");
		
		if(usrPwd == null || usrPwd.isEmpty())
			throw new InvalidRegistrationException("A password has to be provided");
		
		if(mail == null || mail.isEmpty())
			throw new InvalidRegistrationException("An email has to be provided");
		
		URL registerUrl = new URL(baseUri, "/create_user?username=" + usrEmail + "&password=" + usrPwd + "&email=" + usrEmail);
		JsonReader reader = new JsonReader(new InputStreamReader(registerUrl.openStream()));
		
		try{
			resp = gson.fromJson(reader, IApiResponse.class);
		}
		finally{
			reader.close();
		}
		
		if(! resp.isSuccess())
			throw new InvalidRegistrationException(resp.getMessage());
	}
	
	public boolean tryCreateUser(String usrEmail, String usrPwd, String mail) {
		try{
			this.createUser(usrEmail, usrPwd, mail);
		}catch(Exception e){
			return false;
		}
		return true;
	}
	
	public boolean tryLogin(String usrEmail, String usrPwd) throws IOException{
		try{
			this.Login(usrEmail, usrPwd);
		}
		catch(Exception ex){
			return false;
		}
		return true;
		
	}
	

	
	//Main de démonstration j'écrirai la doc demain
	public static void main(String[] args) throws IOException {
		WebApi webApi = new WebApi();

		boolean succesRegistration = webApi.tryCreateUser("tata", "DeOOOOufeeeazeaze" , "toto@totol.ocm");
	    
		boolean successLogin = webApi.tryLogin("toto", "DeOOOOufeeeazeaze" );
		
		ArrayList<Question> getQuizeQuestion = webApi.getQuestion();
		
		
	}
	
}


