package WebService;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class IServeurResponseAdapter<T> implements JsonDeserializer<IApiResponse<T>>{
	//Work around pour l'impossibilité de récupérer un type générique en runtime
	final Class<T> typeParameterClass;
	
	public IServeurResponseAdapter(Class<T> typeParameterClass){
		super();
		this.typeParameterClass = typeParameterClass;
		
	}


	@Override
	public IApiResponse<T> deserialize(JsonElement json, Type typeOfT,
			JsonDeserializationContext context) throws JsonParseException {
	
		JsonObject jsonObject =  json.getAsJsonObject();
		
		if (jsonObject.has("erreur")) {
			String s = jsonObject.get("erreur").getAsString();
			return new ApiResponse<T>(false,s ,null);
		}
		else {
			T reponse = null;
			context.deserialize(jsonObject, typeParameterClass);
			return new ApiResponse<T>(true, "",reponse);
		}
	}
}
