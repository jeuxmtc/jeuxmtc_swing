package WebService;

public class ApiResponse<T> implements IApiResponse<T> {
	   private boolean success;
	   private String message;
	   private T response; 
	   
	public ApiResponse(boolean success,String message, T response){
		this.success = success;
		this.response = response;
		this.message = message;
	}
	   
	@Override
	public boolean isSuccess() {
		return this.success;
	}

	@Override
	public T getObject() {
		// TODO Auto-generated method stub
		return response;
	}
	
	public String getMessage(){
		return this.message;
	}
	
}