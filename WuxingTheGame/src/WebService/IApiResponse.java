package WebService;

public  interface IApiResponse<T> {
	public boolean isSuccess();
	public T getObject();
	public String getMessage();
}
