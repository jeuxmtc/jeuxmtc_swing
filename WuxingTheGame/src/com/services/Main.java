package com.services;

import metier.Question;

/**
 * Created with IntelliJ IDEA.
 * User: korat
 * Date: 16/12/2013
 * Time: 01:27
 * To change this template use File | Settings | File Templates.
 */
// this class and file should be deleted, its purpose is just for testing
public class Main {

    public static void main(String[] args) {
        System.out.println("trying to retrive 2 articles\n");
        JQuestions jQ2 = new JQuestions(2);
        Question[] questions2 = jQ2.getQuestions();
        for (Question question : questions2) {
            System.out.println(question.getIntitule());
            System.out.println(question.getExplaination());
            System.out.println(question.getFixedElements().get(0).getUrl_image());
            System.out.println(question.getFixedElements().get(0).getID());
            System.out.println(question.getFixedElements().get(1).getUrl_image());
            System.out.println(question.getFixedElements().get(1).getID());
            System.out.println(question.getFixedElements().get(2).getID());
            System.out.println(question.getFixedElements().get(3).getID());
            System.out.println(question.getFixedElements().get(4).getID());
            System.out.println("size :"+question.getMovableElements().size());
            System.out.println(question.getMovableElements().get(0).value.getUrl_image());
            System.out.println(question.getMovableElements().get(0).value.getID());
            System.out.println(question.getMovableElements().get(question.getMovableElements().size()-1).value.getID());
            System.out.println("\n");
        }
        System.out.println("\n\n\n\ntrying to retrieve all the articles\n");
        JQuestions jQ = new JQuestions();
        Question[] questions = jQ.getQuestions();
        System.out.println("Fetched "+questions.length+" questions");
        /*
        for (Question question : questions){
            System.out.println(question.getIntitule());
        } */
    }

}
