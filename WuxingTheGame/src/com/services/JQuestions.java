package com.services;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import metier.Question;

/**
 * Created with IntelliJ IDEA.
 * User: korat
 * Date: 16/12/2013
 * Time: 00:21
 * To change this template use File | Settings | File Templates.
 *
 * Usage:  simply instantiate it and call the method getQuestions on this instance
 * example
 * -- case 1: no limit required
 *         JQuestion jQ = new JQuestion();
 *        Questions[] questions =  jQ.getQuestions();
 * -- case 2: fetch only the first n articles
 *         JQuestion jQ = new JQuestion(n);
 *         Questions[] questions = jQ.getQuestions();
 *
 * After that, you can do anything you want with each object in questions array, using its properties, given that, it is a Question object.
 */
public class JQuestions {
    private int number;
    private String JSonURL;


    /**
     * Default constructor: that is, we are not restricted to any limit of objects to fetch
     */
    public JQuestions(){
        JSonURL = "http://kai23.fr:9000/questions";
    }

    /**
     *
     * @param number the maximum of questions to fetch, if we are restricted to any limit
     */
    public JQuestions(int number){
        this.number = number;
        JSonURL = "http://kai23.fr:9000/questions?nb="+this.number;

    }

    /**
     *  This function goal is to simply fetch the json data from <em>JSonURL</em> and deserialize them.
     * @return Question[] an array containing the objects fetched and mapped to the domain class Question
     */
    public Question[] getQuestions(){
        Question questions[] = null;
        try{
            // create a client and then a web resource
            Client client = Client.create();
            WebResource webResource = client.resource(JSonURL);
            // create a response object with the specified accepted Media-Type
            ClientResponse clientResponse = webResource.accept("application/json").get(ClientResponse.class);
            if (clientResponse.getStatus() != 200){
                throw new RuntimeException("Failed to fetch data :: HTTP request status: "+ clientResponse.getStatus());
            }
            // retrieve the json data
            String jsonData = clientResponse.getEntity(String.class);
            // Use gson object to deserialize jsonData String object
            Gson gson = new Gson();
            // Deserialize the json data
            questions = gson.fromJson(jsonData,Question[].class);


        }catch (Exception e){
            e.printStackTrace();
        }
        // the function returns the quetions array, which should contain objects of type Question
        return questions;

    }
}
