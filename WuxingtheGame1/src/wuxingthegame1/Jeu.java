package wuxingthegame1;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;  
import metier.Question;

/**
 *
 * @author Alexandre
 */
public class Jeu extends javax.swing.JPanel {
    Frame frame;
    int objetS = -1;
    String iconElement1, iconElement2, iconElement3, iconElement4, iconElement5;
    String iconObject1, iconObject2, iconObject3, iconObject4;
    String iconObjectin1, iconObjectin2, iconObjectin3, iconObjectin4, iconObjectin5;
    String iconCur = "";
    
    // gestion timer
   private Timer timer;
   private Boolean firstTimerLaunch;
   private int currentTime = 0;
   private int kDefaultTimerTimeInSeconds = 20;
   private int kDefaultAnswerScore = 100;
   private int kDefaultMaxAnswerScore = 150;
   private float score = 0;    
   
   private int nbElement1=0;
   private int nbElement2=0;   
   private int nbElement3=0;
   private int nbElement4=0;
   private int nbElement5=0;
   private Boolean available1=true;
   private Boolean available2=true;
   private Boolean available3=true;
   private Boolean available4=true; 
   private Boolean available5=true;
   
   Question q1;
   
    public Jeu(Frame f) {
        initComponents();
        frame = f;
        firstTimerLaunch = true;
        timerLabel.setText("TEMPS : "+ kDefaultTimerTimeInSeconds + "s");
        
        init();
        
        // no border
        ButtonObjet1.setBorder(null);
        ButtonObjet4.setBorder(null);
        ButtonObjet2.setBorder(null);
        ButtonObjet3.setBorder(null);
        ButtonObjet5.setBorder(null);
        ButtonElement1.setBorder(null);
        ButtonElement2.setBorder(null);
        ButtonElement3.setBorder(null);    
        ButtonElement4.setBorder(null);
        ButtonElement5.setBorder(null);
        LabelElement11.setBorder(null);
        LabelElement12.setBorder(null);
        LabelElement13.setBorder(null);
        LabelElement14.setBorder(null);
        LabelElement15.setBorder(null);
        LabelElement21.setBorder(null);
        LabelElement22.setBorder(null);
        LabelElement23.setBorder(null);
        LabelElement24.setBorder(null);
        LabelElement25.setBorder(null);
        LabelElement31.setBorder(null);
        LabelElement32.setBorder(null);
        LabelElement33.setBorder(null);
        LabelElement34.setBorder(null);
        LabelElement35.setBorder(null);
        LabelElement41.setBorder(null);  
        LabelElement42.setBorder(null);  
        LabelElement43.setBorder(null);  
        LabelElement44.setBorder(null);  
        LabelElement45.setBorder(null);  
        LabelElement51.setBorder(null);
        LabelElement52.setBorder(null);
        LabelElement53.setBorder(null);
        LabelElement54.setBorder(null);
        LabelElement55.setBorder(null);
        jPanel1.setBorder(null);
        
        System.out.println("Launch Timer");
        firstTimerLaunch = false;
        currentTime = kDefaultTimerTimeInSeconds;
        reminder(1000);
        
        q1 = frame.getQuestion1();
    }
    
    public void init()
    {
        // Elements
        //ButtonElement1.setIcon(new javax.swing.ImageIcon(getClass().getResource( q1.getFixedElements().get(0).getUrl_image() )));
        ButtonElement1.setIcon(new javax.swing.ImageIcon(getClass().getResource( "/ressources/Element_Feu.png" )));
        ButtonElement2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/Element_Bois.png")));
        ButtonElement3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/Element_Eau.png")));
        ButtonElement4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/Element_Metal.png")));
        ButtonElement5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/Element_Terre.png")));
        
        //Objects
        ButtonObjet1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/coeur.png")));
        ButtonObjet2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/poumon.png")));
        ButtonObjet3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/coeur.png")));
        ButtonObjet4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/coeur.png")));
        ButtonObjet5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/coeur.png")));
        
        LabelElement11.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement12.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement13.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement14.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement15.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement21.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement22.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement23.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement24.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement25.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement31.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement32.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement33.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement34.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement35.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement41.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement42.setIcon(new javax.swing.ImageIcon(getClass().getResource(""))); 
        LabelElement43.setIcon(new javax.swing.ImageIcon(getClass().getResource(""))); 
        LabelElement44.setIcon(new javax.swing.ImageIcon(getClass().getResource(""))); 
        LabelElement45.setIcon(new javax.swing.ImageIcon(getClass().getResource(""))); 
        LabelElement51.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement52.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement53.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement54.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        LabelElement55.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        
        nbElement1=0;
        nbElement2=0;   
        nbElement3=0;
        nbElement4=0;
        nbElement5=0;
        available1=true;
        available2=true;
        available3=true;
        available4=true; 
        available5=true;
        setDefaultCursor();
    }
    
    private void setIconCursor()
    {
        ImageIcon icon = new ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png"));
        frame.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(icon.getImage(),new Point(0,0),"cursorName"));
    }
    
    private void setDefaultCursor()
    {
        frame.setCursor(Cursor.DEFAULT_CURSOR);
    }
    
    public void dropIcon(int i)
    {
        if (i == 1)
            ButtonObjet1.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        if (i == 2)
            ButtonObjet2.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        if (i == 3)
            ButtonObjet3.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        if (i == 4)
            ButtonObjet4.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));   
        if (i == 5)
            ButtonObjet5.setIcon(new javax.swing.ImageIcon(getClass().getResource(""))); 
    }
    
    // Gestion Timer
    public void reminder(int seconds) {
        timer = new Timer();
        timer.schedule(new RemindTask(), seconds);
    }

    class RemindTask extends TimerTask {
        public void run() {
            currentTime--;
            if (currentTime >= 0) {
            	timerLabel.setText("TEMPS : "+ currentTime +"s");
                timer.cancel(); //Terminate the timer thread
            	reminder(1000);
            } else {
            	timer.cancel();
            }
            
        }
    }
	
    public void updateScoreLabel() {

            if (currentTime > 0) {
                    score += (currentTime * (kDefaultMaxAnswerScore - kDefaultAnswerScore) / kDefaultTimerTimeInSeconds) + kDefaultAnswerScore;
            } else {
                    score += kDefaultAnswerScore;
            }

            scoreLabel.setText("SCORE : "+score);
    }

    public void resetTimerLabel() {

            currentTime = kDefaultTimerTimeInSeconds;
            timerLabel.setText("TEMPS : "+currentTime+"s");

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        jButton2 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        LabelElement24 = new javax.swing.JLabel();
        LabelElement22 = new javax.swing.JLabel();
        LabelElement35 = new javax.swing.JLabel();
        LabelElement34 = new javax.swing.JLabel();
        LabelElement33 = new javax.swing.JLabel();
        LabelElement32 = new javax.swing.JLabel();
        LabelElement11 = new javax.swing.JLabel();
        LabelElement21 = new javax.swing.JLabel();
        LabelElement23 = new javax.swing.JLabel();
        LabelElement12 = new javax.swing.JLabel();
        LabelElement44 = new javax.swing.JLabel();
        LabelElement13 = new javax.swing.JLabel();
        LabelElement25 = new javax.swing.JLabel();
        LabelElement14 = new javax.swing.JLabel();
        LabelElement15 = new javax.swing.JLabel();
        LabelElement43 = new javax.swing.JLabel();
        LabelElement53 = new javax.swing.JLabel();
        LabelElement52 = new javax.swing.JLabel();
        LabelElement42 = new javax.swing.JLabel();
        LabelElement54 = new javax.swing.JLabel();
        LabelElement55 = new javax.swing.JLabel();
        LabelElement45 = new javax.swing.JLabel();
        LabelElement31 = new javax.swing.JLabel();
        LabelElement41 = new javax.swing.JLabel();
        LabelElement51 = new javax.swing.JLabel();
        ButtonElement3 = new javax.swing.JButton();
        ButtonElement5 = new javax.swing.JButton();
        ButtonElement1 = new javax.swing.JButton();
        ButtonElement4 = new javax.swing.JButton();
        ButtonElement2 = new javax.swing.JButton();
        jButtonReset = new javax.swing.JButton();
        ButtonObjet5 = new javax.swing.JButton();
        ButtonObjet1 = new javax.swing.JButton();
        ButtonObjet4 = new javax.swing.JButton();
        ButtonObjet2 = new javax.swing.JButton();
        ButtonObjet3 = new javax.swing.JButton();
        jButtonValidate = new javax.swing.JButton();
        ButtonQuit = new javax.swing.JButton();
        jLabelFond = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        scoreLabel = new javax.swing.JLabel();
        timerLabel = new javax.swing.JLabel();
        questionLabel = new javax.swing.JLabel();
        consigneLabel = new javax.swing.JLabel();
        indiceLabel = new javax.swing.JLabel();
        LabelIconConsigne = new javax.swing.JLabel();
        LabelIconIndice = new javax.swing.JLabel();
        LabelTextConsigne = new javax.swing.JLabel();
        LabelTextIndice = new javax.swing.JLabel();

        jToolBar1.setRollover(true);

        jButton2.setText("jButton2");

        setBackground(new java.awt.Color(222, 41, 16));
        setForeground(new java.awt.Color(222, 41, 16));
        setPreferredSize(new java.awt.Dimension(1280, 720));

        jPanel1.setBackground(new java.awt.Color(222, 41, 16));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setLayout(null);

        LabelElement24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement24.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement24);
        LabelElement24.setBounds(170, 160, 90, 90);

        LabelElement22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement22);
        LabelElement22.setBounds(220, 30, 90, 90);

        LabelElement35.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement35.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement35);
        LabelElement35.setBounds(650, 90, 110, 90);

        LabelElement34.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement34.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement34);
        LabelElement34.setBounds(530, 90, 110, 90);

        LabelElement33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement33.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement33);
        LabelElement33.setBounds(680, 10, 100, 90);

        LabelElement32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement32.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement32);
        LabelElement32.setBounds(590, -10, 100, 90);

        LabelElement11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement11);
        LabelElement11.setBounds(70, 320, 100, 100);

        LabelElement21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement21);
        LabelElement21.setBounds(140, 70, 90, 90);

        LabelElement23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement23);
        LabelElement23.setBounds(290, 70, 90, 90);

        LabelElement12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement12);
        LabelElement12.setBounds(150, 270, 110, 100);

        LabelElement44.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement44.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement44);
        LabelElement44.setBounds(930, 170, 90, 90);

        LabelElement13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement13);
        LabelElement13.setBounds(230, 330, 100, 90);

        LabelElement25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement25.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement25);
        LabelElement25.setBounds(270, 160, 100, 90);

        LabelElement14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement14);
        LabelElement14.setBounds(90, 420, 100, 90);

        LabelElement15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement15);
        LabelElement15.setBounds(210, 420, 100, 90);

        LabelElement43.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement43.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement43);
        LabelElement43.setBounds(1050, 70, 100, 90);

        LabelElement53.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement53.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement53);
        LabelElement53.setBounds(1100, 330, 90, 90);

        LabelElement52.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement52.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement52);
        LabelElement52.setBounds(1020, 270, 100, 100);

        LabelElement42.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement42.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement42);
        LabelElement42.setBounds(970, 10, 100, 110);

        LabelElement54.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement54.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement54);
        LabelElement54.setBounds(970, 420, 100, 100);

        LabelElement55.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement55.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement55);
        LabelElement55.setBounds(1080, 420, 100, 100);

        LabelElement45.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement45.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement45);
        LabelElement45.setBounds(1030, 170, 100, 90);

        LabelElement31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement31.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement31);
        LabelElement31.setBounds(510, 0, 100, 90);

        LabelElement41.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement41.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement41);
        LabelElement41.setBounds(890, 70, 100, 90);

        LabelElement51.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement51.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(LabelElement51);
        LabelElement51.setBounds(950, 330, 90, 90);

        ButtonElement3.setBackground(new java.awt.Color(222, 41, 16));
        ButtonElement3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonElement3.setContentAreaFilled(false);
        ButtonElement3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonElement3ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonElement3);
        ButtonElement3.setBounds(540, 0, 190, 170);

        ButtonElement5.setBackground(new java.awt.Color(222, 41, 16));
        ButtonElement5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonElement5.setContentAreaFilled(false);
        ButtonElement5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonElement5ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonElement5);
        ButtonElement5.setBounds(970, 310, 190, 180);

        ButtonElement1.setBackground(new java.awt.Color(222, 41, 16));
        ButtonElement1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonElement1.setContentAreaFilled(false);
        ButtonElement1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonElement1ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonElement1);
        ButtonElement1.setBounds(100, 310, 190, 180);

        ButtonElement4.setBackground(new java.awt.Color(222, 41, 16));
        ButtonElement4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonElement4.setContentAreaFilled(false);
        ButtonElement4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonElement4ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonElement4);
        ButtonElement4.setBounds(920, 60, 190, 180);

        ButtonElement2.setBackground(new java.awt.Color(222, 41, 16));
        ButtonElement2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonElement2.setContentAreaFilled(false);
        ButtonElement2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonElement2ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonElement2);
        ButtonElement2.setBounds(160, 60, 190, 180);

        jButtonReset.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/reset.png"))); // NOI18N
        jButtonReset.setBorder(null);
        jButtonReset.setContentAreaFilled(false);
        jButtonReset.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButtonReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonResetActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonReset);
        jButtonReset.setBounds(470, 520, 161, 60);

        ButtonObjet5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonObjet5.setContentAreaFilled(false);
        ButtonObjet5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonObjet5ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonObjet5);
        ButtonObjet5.setBounds(710, 340, 170, 140);

        ButtonObjet1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonObjet1.setContentAreaFilled(false);
        ButtonObjet1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonObjet1ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonObjet1);
        ButtonObjet1.setBounds(450, 190, 170, 140);

        ButtonObjet4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonObjet4.setContentAreaFilled(false);
        ButtonObjet4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonObjet4ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonObjet4);
        ButtonObjet4.setBounds(530, 340, 170, 140);

        ButtonObjet2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonObjet2.setContentAreaFilled(false);
        ButtonObjet2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonObjet2ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonObjet2);
        ButtonObjet2.setBounds(630, 190, 170, 140);

        ButtonObjet3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonObjet3.setContentAreaFilled(false);
        ButtonObjet3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonObjet3ActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonObjet3);
        ButtonObjet3.setBounds(360, 340, 160, 140);

        jButtonValidate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/valider.png"))); // NOI18N
        jButtonValidate.setBorder(null);
        jButtonValidate.setContentAreaFilled(false);
        jButtonValidate.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButtonValidate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonValidateActionPerformed(evt);
            }
        });
        jPanel1.add(jButtonValidate);
        jButtonValidate.setBounds(640, 520, 161, 60);

        ButtonQuit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/quitter.png"))); // NOI18N
        ButtonQuit.setBorder(null);
        ButtonQuit.setContentAreaFilled(false);
        ButtonQuit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        ButtonQuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonQuitActionPerformed(evt);
            }
        });
        jPanel1.add(ButtonQuit);
        ButtonQuit.setBounds(0, 0, 150, 60);

        jLabelFond.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/Dragon_Central.png"))); // NOI18N
        jPanel1.add(jLabelFond);
        jLabelFond.setBounds(340, 160, 610, 350);

        jPanel2.setBackground(new java.awt.Color(255, 201, 14));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));

        scoreLabel.setText("SCORE : 0");

        timerLabel.setText("TEMPS :");

        questionLabel.setText("QUESTION : 1/15");

        consigneLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        consigneLabel.setText("CONSIGNE");

        indiceLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        indiceLabel.setText("INDICE");

        LabelIconConsigne.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelIconConsigne.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/info.png"))); // NOI18N

        LabelIconIndice.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelIconIndice.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/hint.png"))); // NOI18N

        LabelTextConsigne.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelTextConsigne.setText("Placer les phénomènes de même nature dans les éléments.");

        LabelTextIndice.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelTextIndice.setText("Texte aidant l'utilisateur a placer les phénomènes.");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(98, 98, 98)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(consigneLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                    .addComponent(LabelIconConsigne, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addComponent(LabelTextConsigne, javax.swing.GroupLayout.PREFERRED_SIZE, 376, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(86, 86, 86)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(indiceLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LabelIconIndice, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LabelTextIndice, javax.swing.GroupLayout.PREFERRED_SIZE, 374, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(84, 84, 84)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scoreLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(questionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(timerLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGap(16, 16, 16)
                            .addComponent(timerLabel)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(questionLabel)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scoreLabel))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGap(39, 39, 39)
                            .addComponent(LabelIconConsigne, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGap(16, 16, 16)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(LabelTextIndice, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(indiceLabel)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(LabelIconIndice, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(consigneLabel)
                        .addComponent(LabelTextConsigne, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(47, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel2);
        jPanel2.setBounds(-40, 590, 1340, 130);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1260, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 684, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void ButtonQuitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonQuitActionPerformed
        JPanel p = new Menu(frame);
        frame.setPanel(p);
        setDefaultCursor();
    }//GEN-LAST:event_ButtonQuitActionPerformed

    private void ButtonObjet1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonObjet1ActionPerformed
        if (objetS == -1 && available1)
        {
            objetS = 1;
            iconCur = "coeur";
            setIconCursor();
            dropIcon(objetS);
            available1 = false;
        }
    }//GEN-LAST:event_ButtonObjet1ActionPerformed

    private void ButtonElement2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonElement2ActionPerformed
        if (objetS != -1)
        {
            nbElement2++;
            if (nbElement2 == 1)
                LabelElement21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement2 == 2)
                LabelElement22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement2 == 3)
                LabelElement23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement2 == 4)
                LabelElement24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement2 == 5)
                LabelElement25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            dropIcon(objetS);
            objetS = -1;   
            setDefaultCursor();           
        }
    }//GEN-LAST:event_ButtonElement2ActionPerformed

    private void ButtonElement4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonElement4ActionPerformed
        if (objetS != -1)
        {
            nbElement4++;
            if (nbElement4 == 1)
                LabelElement41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement4 == 2)
                LabelElement42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement4 == 3)
                LabelElement43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement4 == 4)
                LabelElement44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement4 == 5)
                LabelElement45.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            dropIcon(objetS);
            objetS = -1;  
            setDefaultCursor(); 
        }
    }//GEN-LAST:event_ButtonElement4ActionPerformed

    private void ButtonElement1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonElement1ActionPerformed
        if (objetS != -1)
        {
            nbElement1++;
            if (nbElement1 == 1)
                LabelElement11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement1 == 2)
                LabelElement12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement1 == 3)
                LabelElement13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement1 == 4)
                LabelElement14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement1 == 5)
                LabelElement15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            dropIcon(objetS);
            objetS = -1;    
            setDefaultCursor(); 
        }
    }//GEN-LAST:event_ButtonElement1ActionPerformed

    private void ButtonElement5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonElement5ActionPerformed
        if (objetS != -1)
        {
            nbElement5++;
            if (nbElement5 == 1)
                LabelElement51.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement5 == 2)
                LabelElement52.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement5 == 3)
                LabelElement53.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement5 == 4)
                LabelElement54.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement5 == 5)
                LabelElement55.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            dropIcon(objetS);
            objetS = -1;    
            setDefaultCursor(); 
        }
    }//GEN-LAST:event_ButtonElement5ActionPerformed

    private void ButtonObjet4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonObjet4ActionPerformed
        if (objetS == -1 && available4)
        {
            objetS = 4;
            iconCur = "coeur";
            setIconCursor();
            dropIcon(objetS);
            available4 = false;
        }
    }//GEN-LAST:event_ButtonObjet4ActionPerformed

    private void ButtonObjet2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonObjet2ActionPerformed
        if (objetS == -1 && available2)
        {
            objetS = 2;
            iconCur = "poumon";
            setIconCursor();  
            dropIcon(objetS);
            available2 = false;
        }
    }//GEN-LAST:event_ButtonObjet2ActionPerformed

    private void ButtonObjet3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonObjet3ActionPerformed
        if (objetS == -1 && available3)
        {
            objetS = 3;
            iconCur = "coeur";
            setIconCursor();
            dropIcon(objetS);
            available3 = false;
        }
    }//GEN-LAST:event_ButtonObjet3ActionPerformed

    private void ButtonElement3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonElement3ActionPerformed
        if (objetS != -1)
        {
            nbElement3++;
            if (nbElement3 == 1)
                LabelElement31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement3 == 2)
                LabelElement32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement3 == 3)
                LabelElement33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement3 == 4)
                LabelElement34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement3 == 5)
                LabelElement35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            dropIcon(objetS);
            objetS = -1;
            setDefaultCursor();
        }
    }//GEN-LAST:event_ButtonElement3ActionPerformed

    private void jButtonResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonResetActionPerformed
        init();
        objetS = -1;
    }//GEN-LAST:event_jButtonResetActionPerformed

    private void jButtonValidateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonValidateActionPerformed
        init();
        objetS = -1;

        if(firstTimerLaunch) {
                System.out.println("Launch Timer");
                firstTimerLaunch = false;
                currentTime = kDefaultTimerTimeInSeconds;
                reminder(1000);
        } else {
                System.out.println("Validate answer");
                timer.cancel();
                updateScoreLabel();
                resetTimerLabel();
                reminder(1000);
        }
    }//GEN-LAST:event_jButtonValidateActionPerformed

    private void ButtonObjet5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonObjet5ActionPerformed
        if (objetS == -1 && available5)
        {
            objetS = 5;
            iconCur = "coeur";
            setIconCursor();
            dropIcon(objetS);
            available5 = false;
        }
    }//GEN-LAST:event_ButtonObjet5ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonElement1;
    private javax.swing.JButton ButtonElement2;
    private javax.swing.JButton ButtonElement3;
    private javax.swing.JButton ButtonElement4;
    private javax.swing.JButton ButtonElement5;
    private javax.swing.JButton ButtonObjet1;
    private javax.swing.JButton ButtonObjet2;
    private javax.swing.JButton ButtonObjet3;
    private javax.swing.JButton ButtonObjet4;
    private javax.swing.JButton ButtonObjet5;
    private javax.swing.JButton ButtonQuit;
    private javax.swing.JLabel LabelElement11;
    private javax.swing.JLabel LabelElement12;
    private javax.swing.JLabel LabelElement13;
    private javax.swing.JLabel LabelElement14;
    private javax.swing.JLabel LabelElement15;
    private javax.swing.JLabel LabelElement21;
    private javax.swing.JLabel LabelElement22;
    private javax.swing.JLabel LabelElement23;
    private javax.swing.JLabel LabelElement24;
    private javax.swing.JLabel LabelElement25;
    private javax.swing.JLabel LabelElement31;
    private javax.swing.JLabel LabelElement32;
    private javax.swing.JLabel LabelElement33;
    private javax.swing.JLabel LabelElement34;
    private javax.swing.JLabel LabelElement35;
    private javax.swing.JLabel LabelElement41;
    private javax.swing.JLabel LabelElement42;
    private javax.swing.JLabel LabelElement43;
    private javax.swing.JLabel LabelElement44;
    private javax.swing.JLabel LabelElement45;
    private javax.swing.JLabel LabelElement51;
    private javax.swing.JLabel LabelElement52;
    private javax.swing.JLabel LabelElement53;
    private javax.swing.JLabel LabelElement54;
    private javax.swing.JLabel LabelElement55;
    private javax.swing.JLabel LabelIconConsigne;
    private javax.swing.JLabel LabelIconIndice;
    private javax.swing.JLabel LabelTextConsigne;
    private javax.swing.JLabel LabelTextIndice;
    private javax.swing.JLabel consigneLabel;
    private javax.swing.JLabel indiceLabel;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButtonReset;
    private javax.swing.JButton jButtonValidate;
    private javax.swing.JLabel jLabelFond;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel questionLabel;
    private javax.swing.JLabel scoreLabel;
    private javax.swing.JLabel timerLabel;
    // End of variables declaration//GEN-END:variables
}
