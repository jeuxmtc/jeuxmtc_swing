

package wuxingthegame1;

import javax.swing.JPanel;

/**
 *
 * @author Alexandre
 */
public class Explanations extends javax.swing.JPanel {
    Frame frame;
    
    public Explanations(Frame f, float s) {
        initComponents();
        frame = f;
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelExplanations = new javax.swing.JLabel();
        LabelElement14 = new javax.swing.JLabel();
        LabelElement15 = new javax.swing.JLabel();
        LabelElement13 = new javax.swing.JLabel();
        LabelElement12 = new javax.swing.JLabel();
        LabelElement11 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        LabelElement24 = new javax.swing.JLabel();
        LabelElement25 = new javax.swing.JLabel();
        LabelElement23 = new javax.swing.JLabel();
        LabelElement22 = new javax.swing.JLabel();
        LabelElement21 = new javax.swing.JLabel();
        LabelElement34 = new javax.swing.JLabel();
        LabelElement35 = new javax.swing.JLabel();
        LabelElement33 = new javax.swing.JLabel();
        LabelElement32 = new javax.swing.JLabel();
        LabelElement31 = new javax.swing.JLabel();
        LabelElement44 = new javax.swing.JLabel();
        LabelElement45 = new javax.swing.JLabel();
        LabelElement43 = new javax.swing.JLabel();
        LabelElement42 = new javax.swing.JLabel();
        LabelElement41 = new javax.swing.JLabel();
        LabelElement54 = new javax.swing.JLabel();
        LabelElement55 = new javax.swing.JLabel();
        LabelElement53 = new javax.swing.JLabel();
        LabelElement52 = new javax.swing.JLabel();
        LabelElement51 = new javax.swing.JLabel();
        ButtonElement5 = new javax.swing.JButton();
        ButtonElement1 = new javax.swing.JButton();
        ButtonElement2 = new javax.swing.JButton();
        ButtonElement4 = new javax.swing.JButton();
        ButtonElement3 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(222, 41, 16));
        setPreferredSize(new java.awt.Dimension(1280, 720));
        setLayout(null);

        jLabelExplanations.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabelExplanations.setText("Explanations.");
        jLabelExplanations.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        add(jLabelExplanations);
        jLabelExplanations.setBounds(790, 200, 410, 340);

        LabelElement14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement14.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement14);
        LabelElement14.setBounds(100, 550, 100, 90);

        LabelElement15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement15.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement15);
        LabelElement15.setBounds(210, 550, 100, 90);

        LabelElement13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement13);
        LabelElement13.setBounds(230, 470, 100, 100);

        LabelElement12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement12);
        LabelElement12.setBounds(150, 410, 110, 100);

        LabelElement11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement11);
        LabelElement11.setBounds(70, 470, 100, 100);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/suivant.png"))); // NOI18N
        jButton2.setBorder(null);
        jButton2.setContentAreaFilled(false);
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        add(jButton2);
        jButton2.setBounds(1090, 620, 161, 60);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/retourMenu.png"))); // NOI18N
        jButton1.setBorder(null);
        jButton1.setContentAreaFilled(false);
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1);
        jButton1.setBounds(0, 0, 161, 60);

        LabelElement24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement24.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement24);
        LabelElement24.setBounds(30, 320, 110, 100);

        LabelElement25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement25.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement25);
        LabelElement25.setBounds(150, 320, 100, 90);

        LabelElement23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement23);
        LabelElement23.setBounds(170, 220, 100, 90);

        LabelElement22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement22);
        LabelElement22.setBounds(80, 160, 110, 100);

        LabelElement21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement21);
        LabelElement21.setBounds(10, 220, 100, 90);

        LabelElement34.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement34.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement34);
        LabelElement34.setBounds(260, 160, 110, 100);

        LabelElement35.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement35.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement35);
        LabelElement35.setBounds(380, 160, 100, 100);

        LabelElement33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement33.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement33);
        LabelElement33.setBounds(400, 80, 110, 90);

        LabelElement32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement32.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement32);
        LabelElement32.setBounds(300, 20, 110, 90);

        LabelElement31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement31.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement31);
        LabelElement31.setBounds(220, 80, 100, 90);

        LabelElement44.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement44.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement44);
        LabelElement44.setBounds(490, 330, 100, 100);

        LabelElement45.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement45.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement45);
        LabelElement45.setBounds(590, 330, 110, 100);

        LabelElement43.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement43.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement43);
        LabelElement43.setBounds(610, 240, 110, 90);

        LabelElement42.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement42.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement42);
        LabelElement42.setBounds(530, 180, 110, 100);

        LabelElement41.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement41.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement41);
        LabelElement41.setBounds(460, 230, 110, 100);

        LabelElement54.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement54.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement54);
        LabelElement54.setBounds(410, 550, 100, 90);

        LabelElement55.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement55.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement55);
        LabelElement55.setBounds(520, 550, 100, 90);

        LabelElement53.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement53.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement53);
        LabelElement53.setBounds(540, 480, 100, 90);

        LabelElement52.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement52.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement52);
        LabelElement52.setBounds(460, 420, 100, 90);

        LabelElement51.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelElement51.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        add(LabelElement51);
        LabelElement51.setBounds(380, 470, 100, 100);

        ButtonElement5.setBackground(new java.awt.Color(222, 41, 16));
        ButtonElement5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonElement5.setContentAreaFilled(false);
        ButtonElement5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonElement5ActionPerformed(evt);
            }
        });
        add(ButtonElement5);
        ButtonElement5.setBounds(410, 440, 190, 180);

        ButtonElement1.setBackground(new java.awt.Color(222, 41, 16));
        ButtonElement1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonElement1.setContentAreaFilled(false);
        ButtonElement1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonElement1ActionPerformed(evt);
            }
        });
        add(ButtonElement1);
        ButtonElement1.setBounds(100, 440, 190, 180);

        ButtonElement2.setBackground(new java.awt.Color(222, 41, 16));
        ButtonElement2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonElement2.setContentAreaFilled(false);
        ButtonElement2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonElement2ActionPerformed(evt);
            }
        });
        add(ButtonElement2);
        ButtonElement2.setBounds(40, 210, 190, 180);

        ButtonElement4.setBackground(new java.awt.Color(222, 41, 16));
        ButtonElement4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonElement4.setContentAreaFilled(false);
        ButtonElement4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonElement4ActionPerformed(evt);
            }
        });
        add(ButtonElement4);
        ButtonElement4.setBounds(480, 220, 190, 180);

        ButtonElement3.setBackground(new java.awt.Color(222, 41, 16));
        ButtonElement3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ButtonElement3.setContentAreaFilled(false);
        ButtonElement3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonElement3ActionPerformed(evt);
            }
        });
        add(ButtonElement3);
        ButtonElement3.setBounds(270, 60, 190, 170);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/titre_FR.png"))); // NOI18N
        add(jLabel1);
        jLabel1.setBounds(730, 50, 510, 80);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/background.png_500x500.png"))); // NOI18N
        add(jLabel2);
        jLabel2.setBounds(750, 150, 500, 450);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/dragon.png"))); // NOI18N
        add(jLabel3);
        jLabel3.setBounds(100, 100, 530, 540);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        //JPanel p = new Explanation(frame);
        //frame.setPanel(p);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JPanel p = new Menu(frame);
        frame.setPanel(p);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void ButtonElement1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonElement1ActionPerformed
        if (objetS != -1)
        {
            nbElement1++;
            if (nbElement1 == 1)
            LabelElement11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement1 == 2)
            LabelElement12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement1 == 3)
            LabelElement13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement1 == 4)
            LabelElement14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement1 == 5)
            LabelElement15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
        }
    }//GEN-LAST:event_ButtonElement1ActionPerformed

    private void ButtonElement2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonElement2ActionPerformed
        if (objetS != -1)
        {
            nbElement2++;
            if (nbElement2 == 1)
            LabelElement21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement2 == 2)
            LabelElement22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement2 == 3)
            LabelElement23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement2 == 4)
            LabelElement24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement2 == 5)
            LabelElement25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            dropIcon(objetS);
            objetS = -1;
            setDefaultCursor();
        }
    }//GEN-LAST:event_ButtonElement2ActionPerformed

    private void ButtonElement3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonElement3ActionPerformed
        if (objetS != -1)
        {
            nbElement3++;
            if (nbElement3 == 1)
            LabelElement31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement3 == 2)
            LabelElement32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement3 == 3)
            LabelElement33.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement3 == 4)
            LabelElement34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement3 == 5)
            LabelElement35.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            dropIcon(objetS);
            objetS = -1;
            setDefaultCursor();
        }
    }//GEN-LAST:event_ButtonElement3ActionPerformed

    private void ButtonElement4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonElement4ActionPerformed
        if (objetS != -1)
        {
            nbElement4++;
            if (nbElement4 == 1)
            LabelElement41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement4 == 2)
            LabelElement42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement4 == 3)
            LabelElement43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement4 == 4)
            LabelElement44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement4 == 5)
            LabelElement45.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            dropIcon(objetS);
            objetS = -1;
            setDefaultCursor();
        }
    }//GEN-LAST:event_ButtonElement4ActionPerformed

    private void ButtonElement5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonElement5ActionPerformed
        if (objetS != -1)
        {
            nbElement5++;
            if (nbElement5 == 1)
            LabelElement51.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement5 == 2)
            LabelElement52.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement5 == 3)
            LabelElement53.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement5 == 4)
            LabelElement54.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            if (nbElement5 == 5)
            LabelElement55.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ressources/"+ iconCur + ".png")));
            dropIcon(objetS);
            objetS = -1;
            setDefaultCursor();
        }
    }//GEN-LAST:event_ButtonElement5ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonElement1;
    private javax.swing.JButton ButtonElement2;
    private javax.swing.JButton ButtonElement3;
    private javax.swing.JButton ButtonElement4;
    private javax.swing.JButton ButtonElement5;
    private javax.swing.JLabel LabelElement11;
    private javax.swing.JLabel LabelElement12;
    private javax.swing.JLabel LabelElement13;
    private javax.swing.JLabel LabelElement14;
    private javax.swing.JLabel LabelElement15;
    private javax.swing.JLabel LabelElement21;
    private javax.swing.JLabel LabelElement22;
    private javax.swing.JLabel LabelElement23;
    private javax.swing.JLabel LabelElement24;
    private javax.swing.JLabel LabelElement25;
    private javax.swing.JLabel LabelElement31;
    private javax.swing.JLabel LabelElement32;
    private javax.swing.JLabel LabelElement33;
    private javax.swing.JLabel LabelElement34;
    private javax.swing.JLabel LabelElement35;
    private javax.swing.JLabel LabelElement41;
    private javax.swing.JLabel LabelElement42;
    private javax.swing.JLabel LabelElement43;
    private javax.swing.JLabel LabelElement44;
    private javax.swing.JLabel LabelElement45;
    private javax.swing.JLabel LabelElement51;
    private javax.swing.JLabel LabelElement52;
    private javax.swing.JLabel LabelElement53;
    private javax.swing.JLabel LabelElement54;
    private javax.swing.JLabel LabelElement55;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabelExplanations;
    // End of variables declaration//GEN-END:variables
}
